package timer.click.we.de.clicktimer

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.info
import timer.click.we.de.clicktimer.util.parseMillisecondsToReadableString
import java.util.*

class ClickActivity : AppCompatActivity(), AnkoLogger {

    companion object {
        const val REFRESH_TIME: Long = 1000
        const val timeShowImage: Long = 5000
    }

    private lateinit var editText: TextView

    private var maxTime: Long = 10000
    private var currentTime: Long = maxTime
    private var imageTouched: Boolean = false
    private var randomShowTime: Long = 0
    private var imageButton: ImageButton? = null
    private var timerRemoveImage: CountDownTimer? = null
    private var timerChangeTime: CountDownTimer? = null
    private var timerShowImage: CountDownTimer? = null
    private var nextRoundButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        info("Start game")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_click)


        initButton()

        overwriteRandomShowTime()

        setRecordViewText(maxTime)


        initTimer()

        timerShowImage?.start()
        timerChangeTime?.start()
    }

    private fun initButton() {
        info("init Button")
        imageButton = findViewById(R.id.image_button)
        nextRoundButton = findViewById(R.id.next_round)

        imageButton!!.setOnClickListener {
            imageTouched = true
            info("round accomplished")
            removeImage()
            nextRoundButton!!.visibility = View.VISIBLE
        }

        nextRoundButton!!.setOnClickListener {
            nextRoundButton!!.visibility = View.INVISIBLE

            startNextRound()
        }
    }

    private fun initTimer() {

        info("init Timer")
        timerShowImage = timerShowImage(randomShowTime, REFRESH_TIME)
        timerChangeTime = timerChangeTime(maxTime, REFRESH_TIME)
        timerRemoveImage = timerRemoveImage(timeShowImage, REFRESH_TIME)
    }

    private fun getIntentFromActivity(): Intent? {
        return this.intent
    }


    private fun setRecordViewText(value: Long) {
        editText = findViewById(R.id.time_viewer)
        editText.text = parseMillisecondsToReadableString(value)
    }

    private fun timerChangeTime(millisInFuture: Long, countDownInterval: Long): CountDownTimer {
        return object : CountDownTimer(millisInFuture, countDownInterval) {

            override fun onTick(millisUntilFinished: Long) {
                currentTime -= 1000

                setRecordViewText(currentTime)
            }

            override fun onFinish() {
                endGame()
            }
        }
    }

    private fun timerShowImage(millisInFuture: Long, countDownInterval: Long): CountDownTimer {
        return object : CountDownTimer(millisInFuture, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                timerShowImage?.cancel()
                showImage()
            }
        }
    }

    private fun timerRemoveImage(millisInFuture: Long, countDownInterval: Long): CountDownTimer {
        return object : CountDownTimer(millisInFuture, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                endGame()
            }
        }
    }

    private fun endGame() {
        info("end game")
        val lcIntent: Intent? = getIntentFromActivity()

        lcIntent?.putExtra("maxTime", maxTime.toString())

        setResult(Activity.RESULT_OK, lcIntent)

        finish()
    }

    private fun startNextRound() {
        info("Start next round")
        imageButton?.visibility = View.INVISIBLE
        maxTime += maxTime
        Toast.makeText(this@ClickActivity, getString(R.string.next_round), Toast.LENGTH_SHORT).show()

        overwriteRandomShowTime()

        currentTime = maxTime

        info("Max time: $maxTime")
        debug("RandomTime $randomShowTime")

        timerChangeTime?.start()
        timerShowImage?.start()
    }

    private fun overwriteRandomShowTime() {
        randomShowTime = buildRandomLongValue()

        timerShowImage = timerShowImage(randomShowTime, REFRESH_TIME)
        timerChangeTime = timerChangeTime(maxTime, REFRESH_TIME)
    }

    private fun buildRandomLongValue(): Long {
        return Random().nextInt(maxTime.toInt() - 1).toLong()
    }

    private fun showImage() {
        imageButton?.visibility = View.VISIBLE
        timerRemoveImage?.start()
    }

    private fun removeImage() {
        imageButton?.visibility = View.INVISIBLE

        timerRemoveImage?.cancel()
        timerShowImage?.cancel()
        timerChangeTime?.cancel()
    }
}
