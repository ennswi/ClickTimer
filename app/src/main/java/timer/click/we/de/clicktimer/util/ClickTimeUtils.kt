package timer.click.we.de.clicktimer.util

import java.util.concurrent.TimeUnit

fun parseMillisecondsToReadableString(value: Long): String {
    var hours = if (TimeUnit.MILLISECONDS.toHours(value) < 10) "0" + TimeUnit.MILLISECONDS.toHours(value) else TimeUnit.MILLISECONDS.toHours(value).toString()
    var minutes = if (TimeUnit.MILLISECONDS.toMinutes(value) < 10) "0" + TimeUnit.MILLISECONDS.toMinutes(value) else TimeUnit.MILLISECONDS.toMinutes(value).toString()
    var seconds = if (TimeUnit.MILLISECONDS.toSeconds(value) < 10) "0" + TimeUnit.MILLISECONDS.toSeconds(value) else TimeUnit.MILLISECONDS.toSeconds(value).toString()

    if (hours.toLong() > 0) {
        minutes = (minutes.toLong() - hours.toLong() * 60).toString()
        minutes = if (minutes.toLong() < 10) "0$minutes" else minutes
    }
    if (minutes.toLong() > 0) {
        seconds = (seconds.toLong() - minutes.toLong() * 60).toString()
        seconds = if (seconds.toLong() < 10) "0$seconds" else seconds
    }
    return "$hours:$minutes:$seconds"
}