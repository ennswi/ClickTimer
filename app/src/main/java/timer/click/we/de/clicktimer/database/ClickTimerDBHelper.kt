package timer.click.we.de.clicktimer.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

class ClickTimerDBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    object ClickTimerContract {
        // Table contents are grouped together in an anonymous object.
        object ClickTimer : BaseColumns {
            const val TABLE_NAME = "Record"
            const val COLUMN_NAME_ID = "id"
            const val COLUMN_NAME_RECORD = "record_value"
        }
    }


    companion object {
        private const val SQL_CREATE_ENTRIES =
                "CREATE TABLE ${ClickTimerContract.ClickTimer.TABLE_NAME} (" +
                        "${ClickTimerContract.ClickTimer.COLUMN_NAME_ID} INTEGER PRIMARY KEY," +
                        "${ClickTimerContract.ClickTimer.COLUMN_NAME_RECORD} TEXT)"


        private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS ${ClickTimerContract.ClickTimer.TABLE_NAME}"
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "ClickTimer.db"
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented")
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_ENTRIES)
    }

}