package timer.click.we.de.clicktimer

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.info
import timer.click.we.de.clicktimer.database.ClickTimerDBHelper
import timer.click.we.de.clicktimer.util.parseMillisecondsToReadableString

class MainActivity : AppCompatActivity(), AnkoLogger {

    companion object {
        const val REQUEST_CODE = 4
        const val RECORD_DB_ID = "1"
    }

    private var dbHelper: ClickTimerDBHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        info("Start App")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initDBConnection()

        var record = readDBData()

        if (record != null) {
            setRecordViewText(record)
        }

        val startGameButton = findViewById<Button>(R.id.start_gane_button) as Button

        startGameButton.setOnClickListener {
            Toast.makeText(this@MainActivity, getString(R.string.start_game), Toast.LENGTH_SHORT).show()

            val intent = Intent(this, ClickActivity::class.java)

            //intent.putExtra("keyIdentifier", value)
            startActivityForResult(intent, REQUEST_CODE)
        }
    }

    private fun initDBConnection() {
        info("init DB")
        dbHelper = ClickTimerDBHelper(this.baseContext)
    }

    private fun readDBData(): Long? {
        info("read DB DATA")
        var dbRecord: String? = null
        val projection = arrayOf(ClickTimerDBHelper.ClickTimerContract.ClickTimer.COLUMN_NAME_ID, ClickTimerDBHelper.ClickTimerContract.ClickTimer.COLUMN_NAME_RECORD)
        val selection = "${ClickTimerDBHelper.ClickTimerContract.ClickTimer.COLUMN_NAME_ID} = ?"
        val selectionArgs = arrayOf(RECORD_DB_ID)
        val db = dbHelper!!.readableDatabase
        val cursor = db.query(
                ClickTimerDBHelper.ClickTimerContract.ClickTimer.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
        )


        with(cursor) {
            if (moveToNext()) {
                dbRecord = getString(getColumnIndexOrThrow(ClickTimerDBHelper.ClickTimerContract.ClickTimer.COLUMN_NAME_RECORD))
            }
        }
        var returnValue: Long? = dbRecord?.toLong()
        info("Local Record is: $dbRecord")
        return returnValue
    }

    private fun setRecordViewText(value: Long) {
        var recordView = findViewById<TextView>(R.id.max_time_record)

        recordView.text = parseMillisecondsToReadableString(value)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Toast.makeText(this@MainActivity, getString(R.string.end_game), Toast.LENGTH_SHORT).show()
        var maxTime = ""
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    maxTime = data.extras.getString("maxTime")
                }
            }
        }

        if (maxTime.isNotEmpty()) {
            val oldRecord = readDBData()
            if (oldRecord == null || maxTime.toLong() > oldRecord!!) {
                info("new record")
                info("Time: $maxTime")

                setRecordViewText(maxTime.toLong())


                val value = ContentValues().apply {
                    put(ClickTimerDBHelper.ClickTimerContract.ClickTimer.COLUMN_NAME_ID, RECORD_DB_ID.toLong())
                    put(ClickTimerDBHelper.ClickTimerContract.ClickTimer.COLUMN_NAME_RECORD, maxTime)
                }
                val db = dbHelper?.readableDatabase
                db?.replace(ClickTimerDBHelper.ClickTimerContract.ClickTimer.TABLE_NAME, null, value)
            }
        }
    }

    override fun onDestroy() {
        closeDBConnection()
        super.onDestroy()
    }

    private fun closeDBConnection() {
        info("Close DB Connection")
        dbHelper!!.close()
    }
}
